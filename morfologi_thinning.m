huruf=imread('C:\Users\User\Documents\Mochammad Enrique\Program matlab\Acara 10\huruf.png');
gray=rgb2gray(huruf);
[M,N]=size(gray);
H_seg = zeros(M,N);
for k = 1 : M
 for l = 1 : N
 if gray(k,l)<20 
 H_seg(k,l)=1;
 else
 H_seg(k,l)=0;
 end
 end 
end
thinning=bwmorph(H_seg,'thin',25);
skeletoning=bwmorph(H_seg,'skel',70);

% Display GUI
subplot(1,4,1),imshow(gray);
title('Grayscale');
subplot(1,4,2),imshow(H_seg);
title('Segmentation');
subplot(1,4,3),imshow(thinning);
title('Thinning');
subplot(1,4,4),imshow(skeletoning);
title('Skeletoning');